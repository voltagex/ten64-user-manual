# Does it run?

We have compiled a list of popular applications that are of interest to the Ten64 community and methods to run them.

## Distributions
Due to the 'newness' of the Ten64 and DPAA2 hardware, many distributions have not enabled the required drivers, or there are incompatibilies with our bootloader (see [roadmap](/software/roadmap/))

See the [Distributions](/software/distributions) page for information on known interoperability issues.

Most of the distributions that are currently not working natively can be made to work by installing a mainline [kernel](/kernel/) and/or minor bootloader configuration changes.

All of the distributions in this section that have "Yes" in their column can be installed via the [bare metal appliance store](/software/appliancestore/) (known as `baremetal-deploy`) or the [μVirt  appliance store](https://gitlab.com/traversetech/muvirt/-/wikis/ApplianceStoreQuickStart), or another method if noted.

|Distribution  |Native / Bare Metal|VM    | Notes |
|--------      |-------------------|------|-------|
|Debian        |Yes*                  |Yes   | Debian 12 (bookworm), testing and unstable available from appliance store. See [distribution installer application note](/applications/distroinstall/) if you want to use the Debian installer. |
|Fedora        |Yes*                  |Yes   | Version 38 available from appliance store. Minor adjustments required for native boot (implemented by `baremetal-deploy`). For specific details, see [workaround steps](/software/distributions/#fedora-33). See [distribution installer application note](/applications/distroinstall/) if you want to use the Fedora installer. |
|Ubuntu        |Yes*         |Yes   | Ubuntu 20.04 and later cloud image works from NVMe but DPAA2 Ethernet driver (and USB3) is not installed by default. See [steps for Ubuntu 20.10](/software/distributions/#ubuntu-2004-and-2010-steps)
|OpenSuSE      |Yes               |Yes   | Leap 15.4 and Tumbleweed work OK |
|FreeBSD       |Yes! (NEW)          |Yes    | See [FreeBSD preview for Ten64](https://forum.traverse.com.au/t/freebsd-preview-for-ten64/173/5). We anticipate full support from FreeBSD 14.0-STABLE onwards.  |
|OpenBSD       |No                  |No     | Changes to run OpenBSD as VM under investigation |
|Alpine Linux  |No                  |Needs work| There is an [aarch64 ISO](https://alpinelinux.org/downloads/) but no 'cloud' image |
|Gentoo and Funtoo| Yes  | Planned    | Use [`gentoo-setup`](/software/recovery/#gentoo-setup) to create a working system from a stage3 tarball |
|Arch          |Yes                   |Some assembly required     | Kernel Package linux-aarch64 5.11 and later. Use [`arch-setup`](/software/recovery/#arch-setup) to deploy natively.<br/>To assemble a VM image, see [contributed instructions](https://forum.traverse.com.au/t/unofficial-archlinuxarm-as-uvirt-guest/74/3) on the support forum|
|Yocto         | Yes                  | Yes                       | See [meta-traverse](https://gitlab.com/traversetech/meta-traverse) layer for Yocto Kirkstone |
|CentOS 8      |No<br/>Later kernel required |Yes   | CentOS/RHEL kernel does not have Layerscape drivers, need to use a later kernel (e.g install kernel RPMs from Fedora 33), sadly there is no EPEL/'kernel-ml' on aarch64. The other steps required are similar to Fedora.

### Router and Firewall
| Name          |Native/Bare Metal | VM     | Notes      |
|---------------|------------------|--------|------------|
|OpenWrt        | Yes              | Yes    | `armsr` branch (23.05 and later) for disk boot, `layerscape` for NAND.  See [OpenWrt](/software/openwrt/) page in this manual and [OpenWrt upstreaming update](https://forum.traverse.com.au/t/openwrt-upstreaming-update/651) |
|[pfsense](https://www.pfsense.org/)        | No               | Possible? | Might be able to generate an ARM64 installer image from [source](https://github.com/pfsense/pfsense)|
|[IPFire](https://www.ipfire.org/)         | Yes               | Yes   | |
|[VyOS](http://vyos.net/)       | Yes*             | Yes*   | Experimental port - you will need to compile your own image from [vyos-arm64-builder](https://github.com/mcbridematt/vyos-arm64-builder) using a Debian ARM64 instance first |

### Storage and NAS
| Name          | Native/Bare Metal | VM   | Notes        |
|---------------|-------------------|------|--------------|
| [Rockstor](http://rockstor.com/)     | Yes               | Yes  | Supported from Rockstor 4 (openSuSE based). Binaries not published yet, see our [Rockstor](/applications/rockstor) page|
| [OpenMediaVault](https://www.openmediavault.org/) | Yes | Yes | Install Debian and then install OpenMediaVault from their [apt repository](https://openmediavault.readthedocs.io/en/5.x/installation/on_debian.html) |

Rockstor's Rock-On feature has a curated collection of addons which is a good way of deploying many applications - if you are not familiar with containers or Docker, consider starting there.

## Applications
If your application does not publish ARM64 containers directly, sources such as [LinuxServer.io](https://fleet.linuxserver.io/) may be an alternative.

### IoT and Home Automation
See our [ZigBee Home Automation](/applications/zigbee/) application note for useful information on using ZigBee (and similar technolgies, such as Z-Wave) receivers.

| Name          | Recommended Method |
|---------------|--------------------|
| [WebThings](https://webthings.io/) | Use [packaged versions](https://webthings.io/gateway/) for your favourite distribution or the `webthingsio/gateway` container image |
| [Home Assistant](https://www.home-assistant.io/) | Dedicated VM (if dealing with USB, as above), or container |

### Media Servers (DLNA etc.)

| Name                                     | Recommended Method                                                                               |
|------------------------------------------|--------------------------------------------------------------------------------------------------|
| [Emby Media Server](https://emby.media)  | [Download and Install arm64 package for your distribution](https://emby.media/linux-server.html) |

### Collaboration / Chat / Video
| Name          | Recommended Method |
|---------------|--------------------|
| [Jitsi Meet](https://meet.jit.si/)    | [Container](https://github.com/jitsi/docker-jitsi-meet/issues/195#issuecomment-609073129) |

### Personal/Private Cloud
| Name          | Recommended Method   |
|---------------|----------------------|
| [Nextcloud](https://nextcloud.com/)   | [Official container](https://hub.docker.com/_/nextcloud/)            |
| [Duplicati](https://www.duplicati.com/)   | [LinuxServer.io container](https://fleet.linuxserver.io/image?name=linuxserver/duplicati) (not all versions from the [offical Docker](https://hub.docker.com/r/duplicati/duplicati/tags) have arm64)           |

### Networking
| Name          | Recommended Method   |
|---------------|----------------------|
| UniFi controller | [LinuxServer.io container](https://fleet.linuxserver.io/image?name=linuxserver/unifi-controller)       |
| [Pi-Hole](https://pi-hole.net/)       | Dedicated VM or container (with bridged network) |

### Network Security
| Name          | Recommended Method   |
|---------------|----------------------|
| Kismet        | Bare metal or VM     |
| Suricata      | VM / DPAA2 chained (HOWTO pending) |

### Cloud and Edge Computing
| Name          | Recommended Method   |
|---------------|----------------------|
| [K3s](https://k3s.io/)           | VM - we are working on an "appliance" image|
| [KubeEdge](https://kubeedge.io/en/)      | Use Debian VMs as base and deploy with Ansible [playbook](https://gitlab.com/mcbridematt/kubeedge-playbook)|

If you are interested in running cloud-to-edge stacks such as:

* AWS Greengrass
* Azure IoT Edge

Please contact Traverse - we may be able to assist.

### AI
| Name          | Notes                |
|---------------|----------------------|
| TensorFlow Lite | Google's [Coral AI miniPCIe and M.2](https://coral.ai/products/#production-products) works with VFIO passthrough in muvirt |

### Software Defined Radio
* [GNURadio](https://www.gnuradio.org) and [LimeSDR](https://limemicro.com/products/boards/limesdr/) - see the [LimeSDR-FM Streaming Demo](https://gitlab.com/mcbridematt/limesdrfm)

### Development
* [Works on ARM](https://www.worksonarm.com/projects/) has facilitated ARM64 support for many development tools and languages

* [Apache Archiva container for ARM64](https://gitlab.com/matt_traverse/docker-archiva)

## Not on the list?
If the software in question works on the Raspberry Pi 3 or 4, there is a good chance it will run on the Ten64 - you might just need to get the correct packages for your chosen distribution.

We appreciate any information on applications not listed here and welcome issues or merge requests in the [documentation repository](https://gitlab.com/traversetech/ls1088firmware/ten64-user-manual/).