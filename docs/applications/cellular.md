# Cellular Modem (4G/5G) connections under OpenWrt
This guide will show you how to configure a cellular connection in OpenWrt
using ModemManager and LuCI.

This guide is best suited to Qualcomm chipset modems, where ModemManager and qmicli allow access
to the "QMI" command interface while modem connections are active (in comparison to uqmi etc.).

## Configuration under LuCI
This will assume you are starting from the 'default' configuration - which has
several Ethernet ports set up as LAN, and one port as Ethernet WAN.

- In LuCI, go to the network interfaces screen.

- If you have a `wan6` interface, delete it - if your cellular network has IPv6 capability,
this will be handled by ModemManager
![OpenWrt LuCI Interfaces screen - showing WAN6 that should be deleted](/images/openwrt-default-interface-delete-wan6.jpg)

- In the interface screen, select "ModemManager" under "Protocol"
![OpenWrt WAN interface dialog - switch protocol drop down](/images/openwrt-switch-proto-1.jpg)

- Click the "switch protocol" button that appears afterwards.
![OpenWrt WAN interface dialog - really switch protocol row appears](/images/openwrt-switch-proto-2.jpg)

- Once the ModemManager specific options come up, enter your network APN and SIM pin (if any).

![OpenWrt LuCI ModemManager options](/images/openwrt-luci-mm-options.png)

- Click "Save"

- On the top right of the LuCI screen, click on 'Pending Changes' to review

![OpenWrt switching to ModemManager config](/images/openwrt-modemmanager-all-changes.jpg)
- Click "Save and Apply"

## Troubleshooting
Here are a couple of things to check:

* Is your modem signal strength good?

You can check the modem status with `mmcli -m 0`:
```
root@OpenWrt:/# mmcli -m 0
  -----------------------------------
  General  |               dbus path: /org/freedesktop/ModemManager1/Modem/0
           |               device id: 3b37d996ffd67448e7f35cac56af855cde0fdac6
...
  -----------------------------------
  Status   |                    lock: sim-pin2
           |          unlock retries: sim-pin (3), sim-puk (10), sim-pin2 (3), sim-puk2 (10)
           |                   state: connected
           |             power state: on
           |             access tech: lte
           |          signal quality: 65% (recent)
```

More indepth data is available from `qmicli` (Qualcomm chipsets only). Make sure you specify `-p` so it uses the qmi-proxy:
```
root@OpenWrt:/# qmicli -d /dev/cdc-wdm0 -p --nas-get-signal-strength
[/dev/cdc-wdm0] Successfully got signal strength
Current:
        Network 'lte': '-73 dBm'
RSSI:
        Network 'lte': '-73 dBm'
ECIO:
        Network 'lte': '-2.5 dBm'
IO: '-106 dBm'
SINR (8): '9.0 dB'
RSRQ:
        Network 'lte': '-14 dB'
SNR:
        Network 'lte': '0.0 dB'
RSRP:
        Network 'lte': '-107 dBm'
root@OpenWrt:/# qmicli -d /dev/cdc-wdm0 -p --nas-get-signal-info
[/dev/cdc-wdm0] Successfully got signal info
LTE:
        RSSI: '-73 dBm'
        RSRQ: '-14 dB'
        RSRP: '-107 dBm'
        SNR: '1.0 dB'
5G:
        RSRP: '-32768 dBm'
        SNR: '-3276.8 dB'
        RSRQ: '-32768 dB'
```

* It can also be worth tearing down the cellular connection, shutting down ModemManager and restarting the card:

```
ifdown wan
/etc/init.d/ModemManager stop
echo 1 > /sys/class/gpio/gpio376/value && sleep 1 && echo 0 > /sys/class/gpio/gpio376/value

# (watch dmesg for USB modem disconnect and reconnect)

/etc/init.d/ModemManager start
# (wait 30s or so for modemmanager to enumerate the modem)

mmcli -L
# Check to see if the modem has been detected

ifup wan
```

If GPIO376 (LTE reset) does not cause the modem to restart, you may need to use the LTE power pin (GPIO377) instead - see [LTE](/lte/) for other GPIOs.

## Further resources
* [WWAN section on OpenWrt.org](https://openwrt.org/docs/guide-user/network/wan/wwan/start)