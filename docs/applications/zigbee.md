# Home Automation with ZigBee appliance (Hue and Tradfri) control

This application note discusses methods to control ZigBee appliances, such as the Philips Hue and IKEA Tradfri smart lights
with a ZigBee receiver dongle.

## Recommended Software

* [WebThings](https://webthings.io) (formerly Mozilla IoT)
* [Home Assistant](https://www.home-assistant.io)

Both these packages have different strengths - WebThings is not as customizable but has a nice and simple interface, whereas Home Assistant is provides more opportunities for customization.

## Recommended ZigBee dongles

* [ConBee II](https://phoscon.de/en/conbee2) by Dresden Elektronik
* [Digi XStick USB XU-Z11](https://www.mouser.com/ProductDetail/DIGI/XU-Z11?qs=HKtb%252Bt0PK3DGpXkwugUisQ%3D%3D) - at the time of writing this part has been EOL'ed - but lots of stock remains. We have no experience with the suggested replacement (XU3-A11) yet.

If in doubt, buy the ConBee II. The main appeal of the Digi XStick's is it's command compatibility with the 'XBee' line of modules.

## Other recommended hardware

* The Hue [dimmer switch](https://www.philips-hue.com/en-au/p/hue-dimmer-switch/8718696752029) can be used to [factory reset](https://github.com/WebThingsIO/wiki/wiki/HOWTO:-Factory-reset-a-Hue-bulb) ZigBee bulbs (even non-Hue ones).
   This is highly recommended - if you need to rebuild your setup (reinstallation, switching software and/or dongles), this is one of the few ways you can put devices back into pairing mode.


## Notes about ZigBee
* USB3 signals on the USB ports will interfere with 2.4GHz signals such as WiFi and ZigBee, even if your device (the ZigBee dongle) is USB 2.0 only.

    To get around this, we recommend you place your ZigBee dongle on a USB 2.0 _only_ extension cord a small
  distance away from the Ten64 (1.5-3m / 5 - 10ft). Ferrite Cores placed on the USB cord near the Ten64 might also help
  keep noise out of the ZigBee receiver.

## Deployment issues
### Containers
* Using USB devices in containers often requires passing ``--privileged`` flag, handing extra capabilities to the container, passing the entirety of `/dev` or other similarly 'evil' things - which are not appealing if the container host is shared with other applications.

    For this reason, we recommend deploying a virtual machine for this purpose, as a VM + USB passthrough will more effectively isolate the gateway software. You can use a container inside the VM for the actual software itself.

    You might also want to put your gateway on a separate "IoT" network alongside other IoT devices for enhanced security.

* Both Home Assistant and WebThings recommend running containers in "host" networking mode so they have full access to the local
network - this is important if you have plugins that need to do broadcast discovery of LAN devices.

### USB passthrough

Both the ConBee and XStick work with USB passthrough in μVirt. 

Frustratingly, Digi cleared the USB serial field on the XStick so matching is only possible by vendor and product:

```
list usbdevice 'vendor=1027,product=24577'
```

The ConBee has a serial number:
```
list usbdevice 'serial=DE1234567'
```

If you use the web interface to add the passthrough device, it will chose the best, most specific match for the hardware.

## WebThings deployment
ARM64 [packages](https://github.com/WebThingsIO/gateway/releases) are available for Debian, Fedora and Ubuntu. You can also use a [container](https://hub.docker.com/r/mozillaiot/gateway).

There is an "unofficial" (maintained by us) [appliance VM](https://gitlab.com/matt_traverse/mozilla-iot-appliance-builder) which has the WebThings package pre-installed.

## Home Assistant deployment
Ideally we would use the [Home Assistant Operating System ("hass.io")](https://github.com/home-assistant/operating-system), but there is no ARM64 EFI build yet - watch this space.

In the meantime, you can run the Home Assistant container by setting up a VM with Docker installed and using the official [container setup instructions](https://www.home-assistant.io/docs/installation/docker/).

**It is best to setup a dedicated VM for Home Assistant as the container runs in 'host' networking mode**

You will need to pass through your ZigBee tty device - for the ConBee this usually `/dev/ttyACM0`, for the XStick this will be `/dev/ttyUSB0`:

```
docker run -d --name="home-assistant" \
    --restart=always \
    -v /PATH_TO_YOUR_CONFIG:/config \
    -e TZ="Australia/Melbourne" \
    --device=/dev/ttyACM0 \
    --net=host homeassistant/home-assistant:stable
```


Go to `http://containerhost:8123/` to access the Home Assistant interface, which will present a setup wizard on first use.

In the setup wizard, add the "ZigBee Home Automation" integration, which will prompt (and hopefully, autodetect) your ZigBee device.

You can then pair your ZigBee devices by clicking 'Configure' on the ZigBee integration and then the "+" button on the lower right hand corner.