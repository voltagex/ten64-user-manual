# Wireless (Cellular and WiFi) Assembly Guide

<figure>
  <img src="/images/radioguide/ten64_max_wifi.jpg" />
  <figcaption>Ten64 in "max" dual-band WiFi configuration</figcaption>
</figure>

The Ten64's expandability makes it ideal for both cellular and WiFi wireless
applications.

* The Mini-PCIe slots can be used for WiFi radios. One slot (nearest the SSD) 
has a USB2 connection allowing Bluetooth functionality on compatible cards

* The M.2 Key B card slot works with cellular modems. With PCIe and
the 52mm mounting hole position it can support the latest 5G modems, as well
as 4G/LTE modems.

## A quick legal disclaimer
__Just because you can assemble it, does not mean it's legal__, especially if you
wish to resell a product with radios inside.

* Wireless cards that have 5GHz capabilities are regulated in the US and elsewhere. 
Parts of the 5GHz band are shared with radar installations at airports. 
To avoid interfering with these radars, WiFi APs need to scan the shared 
channels to ensure no radars are present.
(See ["Dynamic Frequency Selection"](https://en.wikipedia.org/wiki/Dynamic_frequency_selection)).

    Due to DFS regulations, practically every wireless card consumers can easily buy
(e.g Intel AX200) do not have full access point capabilities on the 5GHz band.
(Some may allow restricted variants such as WiFi Direct). Under Linux, `iw` and other tools
will label such channels as `No-IR` (no **I**ntentional **R**adiation).

    The wireless cards we use for customer deployments typically come "unlocked"
(for example, they will accept any country code input from the driver) and therefore
not sold directly through consumer channels. Therefore, there must be another layer
of enforcement to ensure the wireless functions are not operated outside allowed parameters[^1].

* Some cellular carriers will not allow "unapproved" devices on their network, or
prevent you from obtaining bulk deals on SIM cards if your device has not been
through their specified test processes.

For these reasons, Traverse Technologies does not sell systems with wireless
cards to individual end users at this time.

If you intend to sell a Ten64 device with wireless radios installed, you
will need to follow any applicable regulations in the country/countries
you sell the device in (e.g FCC Modular approval guidelines).

## Antennas and cables - a quick primer

Antennas for cellular and WiFi use generally use a form of [SMA connector](https://en.wikipedia.org/wiki/SMA_connector).

* Cellular antennas use standard SMA where the antenna has the center pin (male).
![SMA connector and antenna view](/images/radioguide/sma_connectors.jpg)
* WiFi antennas use "reverse-polarity" SMA (RP-SMA) where the device has the center pin.
![RP-SMA connector and antenna view](/images/radioguide/rp_sma.jpg)

Mini-PCIe wireless cards use ["U.FL/MHF"](https://en.wikipedia.org/wiki/Hirose_U.FL),
whereas M.2 cards use the much smaller ["MHF4"](http://www.i-pex.com/product/mhf-4).

<figure>
  <img src="/images/radioguide/wireless_cards.jpg" />
  <figcaption>MiniPCIe with U.FL connectors (top), M.2 modem card with MHF4 connectors (bottom)</figcaption>
</figure>

As as general rule, you will want 200mm cables to reach from the wireless cards to the antenna
cables on the left (fan side) of the Ten64 enclosure. (250mm is needed for the cellular card).
You can use shorter (100mm-150mm) for the antenna mounts on the right side.

### Suggested antennas and cables

* WiFi
    * [Taoglas GW.51.5153](https://www.taoglas.com/product/gw-51-5153-5dbi-2-4-5-8ghz-terminal-mount-dipole-antenna/)
        "Terminal Mount" antenna suitable for attaching directly to a Ten64
    * [Taoglas CAB.622 U.FL to RP-SMA 200mm](https://www.taoglas.com/product/cab-622-ipex-mhfiu-fl-to-200mm-1-13-cable-to-rp-smafbkst/)
    * [Taoglas CAB.628 U.FL to RP-SMA 95mm](https://www.taoglas.com/product/cab628-ipex-mhfiu-fl-to-95mm-1-13-cable-to-rp-smafbkst/)
    * [Walsin RFCBA110607SA6B301 U.FL to RP-SMA 70mm](https://www.digikey.com/en/products/detail/walsin-technology-corporation/RFCBA110607SA6B301/4936136) (Digikey P/N 1292-1071-ND)
* Cellular
    * [Taoglas TG.55.8113](https://www.taoglas.com/product/tg-55-8113-5g-terminal-mount-monopole-antenna/)
        "Terminal Mount" antenna suitable for attaching directly to a Ten64
    * [Taoglas MA963](https://www.taoglas.com/product/guardian-ma963-4-in-1-5g4g-mimo-wall/)
        This is an external antenna array (146*134mm) best mounted on a wall, pole or near a window.
        It has 4 x 3 meter (10 foot) cables coming out of it.
    * [Pulse Larsen W9017BD0200 200mm MHF4 to SMA](https://www.mouser.com/ProductDetail/PulseLarsen-Antennas/W9017BD0200?qs=l7cgNqFNU1hy2%2F0Nc%2F72ZQ%3D%3D) (Mouser P/N 673-W9017BD0200)
    * [Pulse Larsen W9017BD0250 250mm MHF4 to SMA](https://www.mouser.com/ProductDetail/PulseLarsen-Antennas/W9017BD0250?qs=l7cgNqFNU1ixYf9hCy9lmw%3D%3D) (Mouser P/N 673-W9017BD0250)
    * [Taoglas CAB.S02 200mm MHF4 to SMA](https://www.taoglas.com/product/cab-s02-ipexmhf4hsc-comp-to-200mm-0-81mm-coax-cable-to-smafbk-jack-straight/)
    * [Taoglas CAB.S01 100mm MHF4 to SMA](https://www.taoglas.com/product/cab-s01-ipexmhf4hsc-comp-to-100mm-0-81mm-coax-cable-smafbk-jack-straight/)

MHF4 cables are very delicate due to the small connector size and cable diameter. They have very
limited 'cycles' (connection and disconnection) as a result. It is a good
idea to order a few more than you think you will need.

### Tools and accessories

We also suggest having an 8mm **wrench** to tighten the SMA connectors to the enclosure.
![8mm SMA wrench](/images/radioguide/sma_wrench.jpg)

**Cable ties** and cable tie mounts are recommended to keep the routing of radio cables
tidy inside the unit.

As the U.FL/MHF4 connectors on the radio modules can be dislodged by bumps in transit,
we recommend applying **silicone adhesive** to the connectors to keep them secure and
in place. _DOWSIL 744_ is one such sealant. The sealant can be easily be removed
later with a craft knife and blade.

## Recommended wireless cards

At the moment we recommend Qualcomm based 4G and 5G modems, and Qualcomm Atheros
WiFi 5/6 cards (ath10k and ath11k family).

See the [Hardware Compatibility List](/hardware/hcl/) for a list of cards that have been
tested.

### Dual Band = One Band At a Time
Note that "dual-band" in the context of WiFi cards means that the card has a single (1)
radio that can operate on either 2.4GHz or 5GHz. It does not mean the card by itself
can work on 2.4GHz and 5GHz simultaneously, so you will need two cards (not necessarily
the same model) to implement a dual band setup.

Recently, "Dual Band Dual Concurrent" cards have appeared which can do dual-band
in a single module. As discussed later in this document, these are generally aimed
at the low end or client markets and often have limitations (such as 2x2 MIMO only, no Multi-SSID).

### Using Half-Height MiniPCIe cards
Some MiniPCIe WiFi cards come in "half-height" format, but the Ten64 has full-height
MiniPCIe slots. Brackets/adaptors to convert half-height to full height can be found
on marketplaces like Amazon and eBay.

<figure>
  <img src="/images/radioguide/minipcie_half_height.jpg" />
  <figcaption>Half Height MiniPCIe Wireless Card, installed using adapter bracket</figcaption>
</figure>

## Assembly Guides

### 5G only - Quectel RM500

The RM500Q-GL has four antenna ports:

| Port | Bands                                                |
|------|------------------------------------------------------|
| ANT0 | Low band primary, mid and high band MIMO1            |
| ANT1 | Mid and high band diversity, N41 primary             |
| ANT2 | Mid and high band MIMO2, N41 and LAA diversity, GNSS |
| ANT3 | 5G High band primary, LTE diversity, N41 MIMO2       |

Most 5G modules with the SDX55 chipset have similar antenna port configurations, check
the datasheet of your module for specific details.

It is best to route antenna ports with low frequency (600-1000MHz) bands to the right side
of the enclosure, while the high frequency antenna ports should be routed to the left side.

This may sound counter-intuitive (as the high frequency ports will have more cable loss), but
this is balanced out by the low frequency ports not getting as much interference from the board itself.

![Quectel RM500Q-GL routing guide](/images/radioguide/rm500q_gl_antenna_config.jpg)

This setup requires these cables/pigtails:

* 1 x 100mm MHF4 to SMA for ANT0
* 1 x 200mm MHF4 to SMA for ANT3
* 2 x 250mm MHF4 to SMA for ANT1 and AN2

### 5G with outdoor antenna
Using an external antenna can improve performance with mid and high cellular bands ("sub-6")
which is important for high throughput. The antenna can be installed outside a building or 
near a window and pointed towards the nearest tower.

You can route the antenna connectors directly to the 3x2 grid at the rear of the Ten64.
Mapping between individual ports on the modem and antenna array isn't as important in this case,
but the datasheet for the antenna might show certain antenna elements being more suitable
for a particular band.

![Taoglas MA963 with Ten64 open (top view)](/images/radioguide/taoglas_ma963_top_view.jpg)
![Taoglas MA963 with Ten64 open (side view)](/images/radioguide/taoglas_ma963_with_ten64.jpg)

This setup uses 200mm MHF4 to SMA cables. 250mm is recommended for the connector mounts
near the centerline of the unit.

### WiFi 5 or 6 - dual band setup
The maximum practical configuration with the Ten64 enclosure is 4x4+3x3 MIMO (e.g 4 MIMO on 5GHz
and 3 MIMO on 2.4GHz). If you don't have many 2.4GHz devices and/or have a smaller space to cover,
2 MIMO on 2.4GHz is sufficient.

![WiFi Dual Band config](/images/radioguide/wifi_dual_band_config.jpg)

This setup requires these cables/pigtails:

* 4 x 70mm or 100mm U.FL to RP-SMA
* 3 x 200mm U.FL to RP-SMA

As the antenna ports on WiFi cards are identical there is no preference to route certain
ports to certain locations (unlike cellular). However, MIMO separation requirements
mean it's not practical to add more antenna ports for use with terminal mount antennas.

### Mixed Configuration - cellular and WiFi
A "full throttle" cellular and WiFi configuration is difficult to achieve with the Ten64
desktop enclosure, as the latest iterations of both technologies demand 4 MIMO/antennas.

There are some options available:

**Use a lower speed grade of each technology, which does not require as many antennas**

LTE Category 12 (600mbit/s max) and below can work with only two antennas.

Some 5G modules also support reduced antenna configurations with a decrease in throughput.
(Check with your carrier and module supplier if they will support such a configuration)

As many WiFi clients only have 2x2 MIMO support, there may not be a significant decrease
in performance when downgrading from 3x3 or 4x4. However, performance with multiple clients
and through RF impediments such as walls may decrease faster.

**Use an external (not attached to the unit) antenna**

This works well for cellular technologies, where better reception can be obtained by having an
antenna outdoors or near a window.

**Use a dual-band concurrent WiFi radio and/or diplexer to aggregate two radio bands into one
antenna**

Recent WiFi 6 chipsets from various manufacturers (Qualcomm QCA6391, NXP 88W9098, MediaTek MT7915)
support 'dual-band dual-concurrent' (DBDC), allowing a single module to emit 2.4GHz and 5GHz simultaneously,
using the same set of antennas.

At the time of writing (2021-10) these chipsets are very new and may not have full software support
for their DBDC functions. Also, compared to standalone, single-band modules, these chipsets may have
reduced functionality (e.g no multi-SSID) in DBDC mode.

Diplexer's are another option, but engineering them to ensure good performance across both bands
can be difficult.

[^1]: Possible enforcement methods include only allowing regulatory parameters (such as country code and
transmit power) that match the expected deployment of the unit (for example, prohibit changing country code
to non-US for units distributed in the US), or by ensuring the unit can be installed only by
"professional installers". This is not legal advice and only reflects methods we have seen used in the field. 