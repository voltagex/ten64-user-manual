# Installing distributions using their ISO installers

The Ten64 provides the [bare metal appliance store](/software/appliancestore/) as a fast
way of installing many operating systems. However, these typically use "Cloud" images and
so customization options are limited (for example, no custom partitioning).

With the updated bootloader in firmware 0.9 and later, most distribution installers
will work "out of the box", except for a few that need a kernel command line adjustment.

## Ensure SFP mode is set correctly

Distributions with Linux kernel versions earlier than 6.2 cannot safely use the Linux
SFP management code (and related tools like ethtool) without patches.

Please see the [PHY Management Modes Page](/network/managementmodes/) for more information.

If your distribution does not support this, please set the `sfpmode` variable in U-Boot 
before running the installer.

```
=> setenv sfpmode legacy
=> saveenv
=> reset
```

Failure to do this may result in the kernel being stuck when trying to reboot.

## Completely out of the box working installers

* OpenSUSE Tumbleweed

## Installers not working yet

* Ubuntu

## Distributions that need a kernel command line adjustment

Why? Because of a long standing IOMMU setup issue with the DPAA2
networking. The networking processor is booted before the IOMMU
is setup by Linux, so this generates IOMMU faults.

Hopefully this issue is resolved in the near future.

To get around this, on most distributions, use GRUB's edit function
to append the required option to the command line.

### Debian

Debian also needs special steps to force a 'removable media' install
as it cannot write EFI variables.

* At the installer GRUB screen, ensure your desired option is highlighted, and press 'E' to edit.

* Use the arrow keys to scroll to the end of the "linux" line and add "arm-smmu.disable_bypass=n"

<asciinema-player speed="1" start-at="1" src="/asciinema/debian-menu-edit.cast"></asciinema-player>

* Press Ctrl-X to start the installer. It will take around 10 seconds for the installer menu system to start.

* Run the installer

* The installer will present a screen titled "Install the GRUB boot loader", asking if you
  want to install an extra copy of GRUB to the removable media path. Answer YES

![Installer question about GRUB removable media path](/images/grub-option-1.png)

* Answer 'NO' to 'Update NVRAM variables to automatically boot into Debian'

![Installer question about updating NVRAM variables](/images/grub-option-2.png)

* Debian should boot as normal. No further setup is required.

### Fedora instructions

#### Installer setup
<asciinema-player speed="1" start-at="4" src="/asciinema/fedora-38-change-commandline.cast"></asciinema-player>

* At the installer GRUB screen, ensure your desired option is highlighted, and press 'E' to edit.

* Use the arrow keys to scroll to the end of the "linux" line and add "arm-smmu.disable_bypass=0". It will take around 20-30 seconds for the installer to boot.

* Press Ctrl-X to start the installer

* Ignore the "Failed to set new EFI target" warning in the installer and reboot

#### First boot
* On the first reboot, Press "e" at the GRUB menu to edit the command line options

* Insert the string "arm-smmu.disable_bypass=0" after the "linux" command's kernel file name

* Press Ctrl-x to boot Fedora

* On the first Fedora boot, login and get a root prompt via `sudo`

* Open "/etc/default/grub" in a text editor

* Append "arm-smmu.disable_bypass=0" to GRUB_CMDLINE_LINUX

* Save the file and exit your text editor

* Update the GRUB configuration: `sudo grub2-mkconfig -o /boot/grub2/grub.cfg`

* Reboot Fedora

* Fedora should now boot as normal

<script src="/asciinema/asciinema-player.js"></script>
