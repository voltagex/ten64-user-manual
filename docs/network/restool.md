---
layout: page
title: Using restool
permalink: /network/restool/
---

**restool** is a utility for manipulating the DPAA2 dataplane configuration at runtime.

Uses of restool include:

* Creating new network interfaces (DPNIs)
* Viewing Ethernet MAC (DPMAC statistics)
* Creating new DPRC containers to contain DPAA2 objects
* Unlinking/removing existing DPAA2 objects from the current DPRC

### Wrappers and utilities
Some tasks, like creating a network interface, need multiple steps to complete.
Supporting objects like buffers and portals also need to be setup (see ["resource object dependencies"](/network/dpaa2config/#resource-object-dependencies)). 
It is best to use a wrapper utility to interact with restool.

#### Restool built-in wrappers
* `ls-listni` - List DPNIs and their corresponding Linux interface name
* `ls-listmac` - List DPMACs
* `ls-addmux` - Create DPDMUX/data center bridge
* `ls-addsw` - Create a switch object (not applicable to LS1088)

#### Third-party wrappers

* The [dpdk-extras](https://source.codeaurora.org/external/qoriq/qoriq-components/dpdk-extras/tree/dpaa2?h=github.qoriq-os/integration) 
repository from NXP contains `dynamic-dpl.sh` and other helper scripts
* [dpaa2.lua](https://gitlab.com/traversetech/muvirt-feed/-/blob/d27d47329df25bf9bf96aa2ba9009ac38950a85d/dpaa2util/files/dpaa2.lua) library from μVirt.

## Requirements
Restool is supported in mainline kernels from 5.12 onwards. We have also backported it 
to 5.10.x with [this](https://gitlab.com/traversetech/traverse-kernel-patches/-/commit/3c259560e182adc72ea1e655cab0459f47ffaea5)
patchset.

To use restool, the kernel must have the `CONFIG_FSL_MC_UAPI_SUPPORT=y` option set.

The restool source repository can be found at [CodeAurora.org](https://source.codeaurora.org/external/qoriq/qoriq-components/restool/).

Please be aware there are some compatibility issues between restool and MC firmware versions.
A recent restool version (v2.3 or later) should be used for MC >=10.28.

## Usage Examples

### Show drpc contents
```
# restool dprc list
restool dprc show dprc.1
dprc.1 contains 157 objects:
object          label           plugged-state
dpni.10                         plugged
...
dpni.0                          plugged
dpbp.10                         plugged
...
dpbp.0                          plugged
dpmac.10                        plugged
...
dpmac.1                         plugged
dpseci.0                        plugged
dpmcp.35                        plugged
...
dpmcp.1                         plugged
dpio.8                          plugged
...
dpio.1                          plugged
dpcon.80                        plugged
...
dpcon.0                         plugged
```

### Creating a DPRC with passed-through DPNI's
Using `restool -s` activates "script" mode which only returns the IDs of the created objects.

```
#!/bin/sh
DPMAC="dpmac.3"
DPRC=$(restool -s dprc create dprc.1 --label="child VM dprc" \
        --options="DPRC_CFG_OPT_IRQ_CFG_ALLOWED,DPRC_CFG_OPT_SPAWN_ALLOWED,DPRC_CFG_OPT_ALLOC_ALLOWED,DPRC_CFG_OPT_OBJ_CREATE_ALLOWED")

DPNI=$(restool -s dpni create --container="${DPRC}")

restool dprc connect dprc.1 --endpoint1="${DPMAC}" --endpoint2="${DPNI}"
restool dprc assign "${DPRC}" --object="${DPNI}" --plugged=1

DPBP=$(restool -s dpbp create --container="${DPRC}")
restool dprc assign "${DPRC}" --object="${DPBP}" --plugged=1
DPCI=$(restool -s dpci create --container="${DPRC}")
restool dprc assign "${DPRC}" --object="${DPCI}" --plugged=1

for i in $(seq 1 2); do
        THIS_DPMCP=$(restool -s dpmcp create --container="${DPRC}")
        restool dprc assign "${DPRC}" --object="${THIS_DPMCP}" --plugged=1
done
for i in $(seq 1 2); do
        THIS_DPCON=$(restool -s dpcon create --container="${DPRC}" --num-priorities=2)
        restool dprc assign "${DPRC}" --object="${THIS_DPCON}" --plugged=1
done
DPIO=$(restool -s dpio create --container="${DPRC}" --num-priorities=2)
restool dprc assign "${DPRC}" --object="${DPIO}" --plugged=1

restool dprc show "${DPRC}"
```

### ls-listni

```
root@OpenWrt:/# ls-listni
dprc.1/dpni.9 (interface: eth0, end point: dpmac.7)
dprc.1/dpni.8 (interface: eth1, end point: dpmac.8)
dprc.1/dpni.7 (interface: eth2, end point: dpmac.9)
dprc.1/dpni.6 (interface: eth3, end point: dpmac.10)
dprc.1/dpni.5 (interface: eth4, end point: dpmac.3)
dprc.1/dpni.4 (interface: eth5, end point: dpmac.4)
dprc.1/dpni.3 (interface: eth6, end point: dpmac.5)
dprc.1/dpni.2 (interface: eth7, end point: dpmac.6)
dprc.1/dpni.1 (interface: eth8, end point: dpmac.2)
dprc.1/dpni.0 (interface: eth9, end point: dpmac.1)
```

### ls-listmac
```
root@OpenWrt:/# ls-listmac
dprc.1/dpmac.10 (end point: dpni.6)
dprc.1/dpmac.9 (end point: dpni.7)
dprc.1/dpmac.8 (end point: dpni.8)
dprc.1/dpmac.7 (end point: dpni.9)
dprc.1/dpmac.6 (end point: dpni.2)
dprc.1/dpmac.5 (end point: dpni.3)
dprc.1/dpmac.4 (end point: dpni.4)
dprc.1/dpmac.3 (end point: dpni.5)
dprc.1/dpmac.2 (end point: dpni.1)
dprc.1/dpmac.1 (end point: dpni.0)
```

### Creating a DPNI
Note: This assumes you have DPNIs available to create - if you are using the [default DPL](/network/dpaa2config) - they are all allocated already.

```
root@OpenWrt:/# ls-addni --fs-entries=1 dpmac.2
[   49.653234] fsl_dpaa2_eth dpni.1: Adding to iommu group 1
[   49.750369] fsl_dpaa2_eth dpni.1: Probed interface eth1
[   49.755887] fsl_mc_allocator dpbp.10: Adding to iommu group 1
[   49.763027] fsl_mc_allocator dpmcp.35: Adding to iommu group 1
[   49.769303] fsl_mc_allocator dpcon.80: Adding to iommu group 1
[   49.773669] fsl_dpaa2_eth dpni.1 eth1: Link Event: state up
[   49.774154] fsl_dpaa2_eth dpni.1 eth1: Link Event: state up
[   49.775300] fsl_mc_allocator dpcon.79: Adding to iommu group 1
[   49.792405] fsl_mc_allocator dpcon.78: Adding to iommu group 1
[   49.798420] fsl_mc_allocator dpcon.77: Adding to iommu group 1
[   49.804394] fsl_mc_allocator dpcon.76: Adding to iommu group 1
[   49.810356] fsl_mc_allocator dpcon.75: Adding to iommu group 1
[   49.816410] fsl_mc_allocator dpcon.74: Adding to iommu group 1
[   49.822431] fsl_mc_allocator dpcon.73: Adding to iommu group 1
Created interface: eth1 (object:dpni.1, endpoint: dpmac.2)
root@OpenWrt:/# ls-listni
dprc.1/dpni.1 (interface: eth1, end point: dpmac.2)
dprc.1/dpni.0 (interface: eth0, end point: dpmac.7)
```

### Removing a DPNI / unbinding from Linux
Removing a DPNI by itself can be difficult because the DPNI has associated resources (dpcon's, dpbp, dpmcp's as seen above). It is easier to destroy the
entire container - NXP's own scripts do exactly that.

If the DPNI is consumed by a Linux ethernet device, you will need to unbind first:
```
root@OpenWrt:/# ls-listni
dprc.1/dpni.1 (interface: eth1, end point: dpmac.2)
dprc.1/dpni.0 (interface: eth0, end point: dpmac.7)
root@OpenWrt:/# echo 'dpni.1' > /sys/bus/fsl-mc/drivers/fsl_dpaa2_eth/unbind
[  315.294020] fsl_dpaa2_eth dpni.1 eth1: Link Event: state down
root@OpenWrt:/# ifconfig eth1
ifconfig: eth1: error fetching interface information: Device not found
root@OpenWrt:/# restool dpni destroy dpni.1
dpni.1 is destroyed
root@OpenWrt:/# [  325.639520] fsl-mc dpni.1: Removing from iommu group 1
root@OpenWrt:/# ls-listni
dprc.1/dpni.0 (interface: eth0, end point: dpmac.7)
```

### Dumping the current DPL
```
root@OpenWrt:/# restool dprc generate-dpl
/dts-v1/;
/ {
        dpl-version = <10>;
        /*****************************************************************
         * Containers
         *****************************************************************/
        containers {
....
....
....
                connection@10{
                        endpoint1 = "dpni@9";
                        endpoint2 = "dpmac@7";
                };
        };
};
```

### Viewing MAC counters
```
root@OpenWrt:/# restool dpmac info dpmac.5
dpmac version: 4.4
dpmac object id/portal id: 5
plugged state: plugged
endpoint state: 1
endpoint: dpni.3, link is up
DPMAC link type: DPMAC_LINK_TYPE_FIXED
DPMAC ethernet interface: DPMAC_ETH_IF_QSGMII
maximum supported rate 1000 Mbps
Counters:
rx all frames: 70484
rx frames ok: 70484
rx frame errors: 0
rx frame discards: 0
rx u-cast: 70443
rx b-cast: 6
rx m-cast: 35
rx 64 bytes: 21
rx 65-127 bytes: 1033
rx 128-255 bytes: 25
rx 256-511 bytes: 6
rx 512-1023 bytes: 900
rx 1024-1518 bytes: 68499
rx 1519-max bytes: 0
rx frags: 0
rx jabber: 0
rx align errors: 0
rx oversized: 0
rx pause: 0
rx bytes: 104940085
tx frames ok: 35307
tx u-cast: 35291
tx m-cast: 13
tx b-cast: 3
tx frame errors: 0
tx undersized: 0
tx b-pause: 0
tx bytes: 2473258
```

### Viewing DPNI counters
```
root@OpenWrt:/# restool dpni info dpni.3
dpni version: 7.13
dpni id: 3
plugged state: plugged
endpoint state: 1
endpoint: dpmac.5, link is up
link status: 1 - up
mac address: 00:0a:fa:24:24:31
dpni_attr.options value is: 0
num_queues: 1
num_cgs: 1
num_rx_tcs: 1
num_tx_tcs: 1
mac_entries: 16
vlan_entries: 0
qos_entries: 0
fs_entries: 1
qos_key_size: 0
fs_key_size: 56
ingress_all_frames: 70454
ingress_all_bytes: 104656040
ingress_multicast_frames: 3
ingress_multicast_bytes: 240
ingress_broadcast_frames: 8
ingress_broadcast_bytes: 706
egress_all_frames: 35307
egress_all_bytes: 2332012
egress_multicast_frames: 13
egress_multicast_bytes: 1513
egress_broadcast_frames: 3
egress_broadcast_bytes: 1032
ingress_filtered_frames: 189
ingress_discarded_frames: 0
ingress_nobuffer_discards: 0
egress_discarded_frames: 0
egress_confirmed_frames: 35307
```