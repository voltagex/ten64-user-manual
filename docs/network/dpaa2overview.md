# DPAA2 Technology overview

This page provides a more in depth overview of DPAA2 technology.

If you are looking for a more practical overview, see [DPAA2 configuration](dpaa2config.md).

## DPAA2 components
Lets start with an overview of the LS1088 itself:

![LS1088 overview](/images/LS1088A-BLOCK-DIAGRAM.jpg)
_LS1088A block diagram from [NXP LS1088A product page](https://www.nxp.com/products/processors-and-microcontrollers/arm-processors/layerscape-communication-process/qoriq-layerscape-1088a-and-1048a-communications-processor:LS1088A)._

DPAA2 (in the red dashed box) encompasses the network and security functions of the LS1088 - the Ethernet MACs through the WRIOP as well as crypto acceleration, buffering and the programmable accelerator (AIOP).

The configuration of DPAA2 resources is through the _Management Complex_, which abstracts the resources into objects which can be connected together dynamically.

### Wire Rate I/O Processor (WRIOP)
The Wire Rate I/O Processor (WRIOP) is the Ethernet part of DPAA2. It is responsible for receiving and transmitting Ethernet frames, traffic statistics and error reporting. The WRIOP can classify incoming packets and queue them by priority.

### Management Complex
The Management Complex (MC) is a small CPU which assists in the management of the dataplane. It runs an NXP supplied firmware which abstracts the hardware resources into objects, handling the setup, linking and teardown. The MC only handles the _configuration_ - the actual traffic is transacted the buffer manager (QBMan) and WRIOP.

### Queue/Buffer Manager (QBMan)
QBMan provides hardware buffer and queue management, accessible to both the CPU ("General Purpose Processor" in NXP parlance) and accelerators such as AIOP. The operating system will transact Ethernet frames to/from the WRIOP Ethernet MACs via the QBMan.

### Advanced I/O Processor (AIOP)
The AIOP is a programmable, purpose-built accelerator that can be used to perform certain network related tasks far more efficiently than a general purpose (GPP, e.g Cortex-A53) core. Examples of use cases that could be accelerated by AIOP (to name a few) include network address translation, traffic offloading and VPN en/decapsulation.

AIOP cores are based on the PowerPC e200 embedded core and are designed to run tasks rather than general purpose operating systems. AIOP is not fully autonomous - the GPP must still perform management functions for the associated MACs, as well as loading the code into AIOP.

At the moment, the only toolchain for AIOP is NXP's CodeWarrior.

For more information, see the _Advanced I/O Processor User Manual_ attachment to the Layerscape SDK User Guide.

### SEC engine
The security engine provides acceleration for some popular protocols such as IPSec and SSL/TLS, related standards such as RSA, DES and AES, as well as some operations typical of wireline and wireless networking such as CRC's, digest authentication and random number generators.

SEC engine has a long evolution alongside the QorIQ family - check the NXP/Freescale resources if you wish to learn more.

LS1088 users also have access to [ARMv8 cryptography extensions](http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0501f/CACIHEDB.html) which may be better suited for certain applications.

## DPAA2 objects
The DPAA2 hardware is represented as objects (abstracted by the Management Complex) which can be categorized into four categories.
<div style="text-align: center">
<table style="text-align: center !important;">
  <tr>
    <th>Network Objects</th>
    <th>Infrastructure Objects</th>
  </tr>
  <tr>
    <td>
      <ul>
        <li>DPNI</li>
        <li>DPMAC</li>
        <li>DPDMUX</li>
      </ul>
    </td>
    <td>
      <ul>
        <li>DPIO</li>
        <li>DPBP</li>
        <li>DPCON</li>
      </ul>
    </td>
  </tr>
  <tr>
    <th>Accelerator Objects</th>
    <th>Management/Control Objects</th>
  </tr>
  <tr>
    <td>
      <ul>
        <li>DPSECI</li>
      </ul>
    </td>
    <td>
      <ul>
        <li>DPCI</li>
        <li>DPRC</li>
        <li>DPMCP</li>
      </ul>
    </td>
  </tr>
</table>
</div>
_Note_: Other objects such as DPSW,DPLAG,DPDCEI exist in other DPAA2 family members (LS2088,LX2160 and LA1575), these have been omitted here.

### Network Objects

#### DPNI
DPNI represents an Ethernet controller - the "eth" interface in your operating system corresponds to a DPNI.
#### DPMAC
DPMAC represents an Ethernet MAC, which connects to the physical interface (PHY) as well as managing MII/MDIO (MC allows multiple degrees of control depending on the demands of the host operating system).
#### DPDMUX
DPDMUX is a simple Ethernet bridge designed for "edge virtual bridging" use cases - such as inter-VM traffic in a virtualization scenario. It is not a full Ethernet switch with learning capabilities - forwarding is based on pre-defined rules.

### Infrastructure Objects
#### DPIO
DPIO provides a portal to the QBMan for the GPP (ARM) cores. DPNI's use DPIO's to send and receive traffic via the QBMan and DPMAC's, as well as other objects such as DPSECI.

A DPIO can serve multiple DPNI's and can be affixed to a CPU core - in typical use there is one DPIO for each core, which allows the processing of network traffic to be efficiently distributed over all the ARM cores.

#### DPBP
DPBP provides management of buffer pools in the QBMan.

#### DPCON
DPCON (Data Path Concentrator) provides scheduling of ingress packets with multiple priority levels.

### Accelerator Objects
#### DPSECI
DPSECI provides a method of queueing tasks for the SEC engine.

### Management/Control Objects
#### DPCI
DPCI provides a method of inter-process communication between different users of the dataplane, such as between GPP and AIOP software.
#### DPRC
DPRC (Data Path Resource Container) provides a level of "containerization" (as the name suggests) for users of DPAA2 objects. For example, a virtualization host can have a DPRC for itself and individual DPRCs for VMs.
#### DPMCP
DPMCP (Data Path MC Portal) provides an interface to the Management Complex - the creation of objects is done through this portal.

## Further reading and references
* Linley Group _Microprocessor Report_, ["Freescale overhauls the dataplane"](https://www.linleygroup.com/newsletters/newsletter_detail.php?num=5363) - paid article but an excellent plain English overview.[^1]

* DPAA2 user manual - attachment to the  [Layerscape SDK User Guide](https://www.nxp.com/docs/en/user-guide/LSDKUG_Rev19.09.pdf).

* The Layerscape SDK User Guide above also contains a DPAA2 introduction in section 8.3 - _DPAA2 Hardware_, as well as information on the Linux driver.

* [DPAA2 Overview](https://www.kernel.org/doc/html/v4.19/networking/dpaa2/overview.html) from Linux kernel documentation.

[^1]: NXP acquired Freescale Semiconductor, and the QorIQ/Layerscape product line in 2016.
