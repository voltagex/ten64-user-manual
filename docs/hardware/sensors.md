# Sensors / Hwmon

The Ten64 board has multiple sensors, with a view to provide a complete picture of system temperatures, voltages and currents.

The sensor IC's are quite new and are not currently in the upstream kernel - we provide a [dkms based package](https://gitlab.com/traversetech/ls1088firmware/traverse-sensors) to compile these out of tree.

## List of sensors

| Sensor     | Bus       | Attribute      | Description                                                        | Units                 |
|------------|-----------|----------------|--------------------------------------------------------------------|-----------------------|
| EMC1704    | I2C1/0x18 | external1_temp | LS1088 Die temperature sensor                                      | Milli-degreesC (10^3) |
| EMC1704    | I2C1/0x18 | external2_temp | Board Temperature sensor Q3 (near NVMe slot)                       | Milli-degreesC (10^3) |
| EMC1704    | I2C1/0x18 | external3_temp | Not used                                                           |                       |
| EMC1704    | I2C1/0x18 | internal_temp  | EMC1704 internal temperature sensor (between uC and SPI-NOR flash) | Milli-degreesC (10^3) |
| EMC1704    | I2C1/0x18 | source_voltage | VIN                                                                | Micro-volts (10^6)    |
| EMC1704    | I2C1/0x18 | sense_voltage  | Not used                                                           |                       |
| EMC1813    | I2C1/0x4c | temp1_input    | EMC1813 internal temperature                                       | Milli-degreesC (10^3) |
| EMC1813    | I2C1/0x4c | temp2_input    | VSC8514 #1 (eth0-eth3) die temperature                             | Milli-degreesC (10^3) |
| EMC1813    | I2C1/0x4c | temp3_input    | VSC8514 #2 (eth4-eth7) die temperature                             | Milli-degreesC (10^3) |
| PAC1934 #1 | I2C1/0x11 | vbus0          | miniPCIe P4 3.3V Sense+                                            | Micro-volts (10^6)    |
| PAC1934 #1 | I2C1/0x11 | vbus1          | miniPCIe P5 3.3V Sense+                                            | Micro-volts (10^6)    |
| PAC1934 #1 | I2C1/0x11 | vbus2          | LTE/M.2B 3.3V Sense+                                               | Micro-volts (10^6)    |
| PAC1934 #1 | I2C1/0x11 | vbus3          | 5V                                                                 | Micro-volts (10^6)    |
| PAC1934 #2 | I2C1/0x1a | vbus0          | DDR VDD (1.2V) voltage                                             | Micro-volts (10^6)    |
| PAC1934 #2 | I2C1/0x1a | vbus1          | DDR VPP (2.5V) voltage                                             | Micro-volts (10^6)    |
| PAC1934 #2 | I2C1/0x1a | vbus2          | DDR VTT (0.6V) voltage                                             | Micro-volts (10^6)    |
| PAC1934 #2 | I2C1/0x1a | vbus3          | 1.8V (OVDD) voltage                                                | Micro-volts (10^6)    |

# Sysfs access
You can read the sensor values via sysfs. The hwmon device names are numbered as they appear in the kernel, so be careful when referencing them.

```
ls -la /sys/class/hwmon/
drwxr-xr-x    2 root     root             0 Oct 15 01:15 .
drwxr-xr-x   58 root     root             0 Jan  1  1970 ..
lrwxrwxrwx    1 root     root             0 Oct 15 01:15 hwmon0 -> ../../devices/platform/soc/2000000.i2c/i2c-0/0-0018/hwmon/hwmon0
lrwxrwxrwx    1 root     root             0 Oct 15 01:15 hwmon1 -> ../../devices/platform/soc/2000000.i2c/i2c-0/0-004c/hwmon/hwmon1
lrwxrwxrwx    1 root     root             0 Oct 15 01:15 hwmon2 -> ../../devices/platform/soc/2000000.i2c/i2c-0/0-0011/hwmon/hwmon2
lrwxrwxrwx    1 root     root             0 Oct 15 01:15 hwmon3 -> ../../devices/platform/soc/2000000.i2c/i2c-0/0-001a/hwmon/hwmon3
```

## Example - uploading sensor values to a time-series database
If you are uploading sensor values to a database, it might be helpful to use the "of_node" symlink to uniquely and consistently identify each hwmon device

For example, the script below can produce formatted key-values for InfluxDB:
```
#!/bin/sh
board_id=$(sed 's/://g' /sys/class/net/eth0/address)
curtime=$(date +%s)
for s in $(ls /sys/class/hwmon); do
  chip_id=$(basename $(realpath "/sys/class/hwmon/${s}/of_node"))
  sensors=$(find "/sys/class/hwmon/${s}/" -maxdepth 1 -perm 0444 ! -name suppliers ! -name consumers ! -name name)
  for x in ${sensors}; do
    valname=$(basename "${x}")
    value=$(cat "${x}")
    #echo "boards.${board_id}.${chip_id}.${valname} ${value} ${curtime}" | nc 192.168.0.16 2003
    data="${chip_id}.${valname},board=${board_id} value=${value} ${curtime}"
    # Print the value
    echo "${data}"
    # Submit to influxdb
    curl -XPOST "http://influxdb:8086/write?precision=s&db=ten64" --data-binary "${data}"
  done
done
```

Gives the output:
```
emc1813@4c.temp3_input,board=000afa2424e5 value=73625 1571103854
emc1813@4c.temp1_input,board=000afa2424e5 value=43125 1571103854
emc1813@4c.temp2_input,board=000afa2424e5 value=63625 1571103854
...
emc1704@18.internal_temp,board=000afa2424e5 value=44000 1571103854
emc1704@18.external2_temp,board=000afa2424e5 value=40000 1571103854
emc1704@18.sense_voltage,board=000afa2424e5 value=0 1571103854
```
