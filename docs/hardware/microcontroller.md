# Ten64 Microcontroller overview

Ten64 boards use an [NXP LPC804](https://www.nxp.com/products/processors-and-microcontrollers/arm-microcontrollers/general-purpose-mcus/lpc800-cortex-m0-plus-/low-cost-microcontrollers-mcus-based-on-arm-cortex-m0-plus-core:LPC80X)
microcontroller (Cortex-M0+ based) to coordinate boot and power management, as well as certain utility functions.

The board MAC address is stored in the microcontrollers EEPROM.

Communication between the host processor (LS1088A) and microcontroller is via I2C.

The microcontroller flash is partitioned into two copies (A and B), allowing "over-the-air" upgrades through I2C.

## Key MCU firmware updates

* v1.1.2 - Ten64 RTM

* v1.1.3 - fix reporting of ATX power status

* v1.1.5 - fix ASLEEP issue exposed by component changes on Ten64 boards built >=2022 (all RevC2 boards, all RevD)

    This is the minimum version these boards will run.

* v1.2 - Introduce `bootcount` and `nextbootpart` scratch registers

## Microcontroller source code

The microcontroller source code can be obtained from the [traversetech/ls1088firmware/ten64-uc-firmware](https://gitlab.com/traversetech/ls1088firmware/ten64-uc-firmware) repository on gitlab.com.

Building can be done with a "baremetal" ARM toolchain (`arm-none-eabi`) with CMake or using NXP's [MCUXpresso](https://www.nxp.com/design/software/development-software/mcuxpresso-software-and-tools-/mcuxpresso-integrated-development-environment-ide:MCUXpresso-IDE) IDE.

## Software utilities

### ten64-controller
[ten64-controller](https://gitlab.com/traversetech/ls1088firmware/ten64-microcontroller-utility) is included in the recovery firmware
and OpenWrt builds.

## Upgrading MCU firmware

There are two versions of each MCU firmware: one for slot A and one for slot B.
This is to account for the fact that each version is executed 'in place' (XIP) from the microcontrollers flash,
hence there is no memory relocation.

