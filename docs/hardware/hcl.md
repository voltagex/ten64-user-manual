# Hardware Compatibility List

This page lists hardware that we (Traverse) test and use with our boards on a
regular basis.

Standard PCIe/USB3/LTE/SFP modules should work, we advise that you test
any third-party modules aggressively before starting any mass deployments.

## DDR SO-DIMMs
See the [DDR](/hardware/ddr) page

## Storage

### M.2 NVMe
<strong>Only PCI Express (e.g NVMe) devices will work in the M.2 Key M (SSD slot)</strong>

There are only two PCIe lanes going to this slot, so high end SSDs with large DRAM caches
(designed to saturate a PCIe3 x4 connection) may not provide much of a performance benefit[^1].

There are many NVMe drives on the market and generally there is no reason for any
particular model not to work in the Ten64. Instead of providing an exhaustive list, we
will list mostly those with interesting attributes (power usage, temperature, special features)

#### General Use

* Samsung 980 (retail) / PM9A1 (OEM)
    * 256GB Model: MZVL2256HCHQ-00B00
    * 1TB Model: MZVL21T0HCLR-00B00

#### Low Power

* Western Digital 'Blue' SN570
* Western Digital 'Blue' SN550
* Western Digital 'Blue' SN500

#### High Write Endurance (e.g NAS , video recording)

* Apacer AP1TPP3480-R (also low power consumption, Long Term Supply/Support)

#### With Power Loss Prevention

* Kingston SEDC1000BM8/960G

#### Small form factor (Key B / "Cellular slot" compatible)

* Western Digital SN520 P/N SDAPMUW-128G-1022 (also Long Term Supply/Support)

#### Self-encryption support (SED / TCG Opal)

(None qualified yet, waiting on bootloader support. Contact us if this is an immediate requirement)

#### Other / misc

A sample of known working devices:

* WD Blue SN500, SN550 (2019 version - the older versions are SATA)
* Crucial P1
* Samsung 960 PRO
* Apacer AS228AP2 series (including 85.DCD650.B011C)
* Patriot P300

### USB

Some older USB2 era drives are known to cause boot stalls in U-Boot as they don't finish initializing by the time U-Boot does a
USB scan (this occurs much faster [relative to ```t=0```] than on PCs of the comparable era).

If you encounter issues with this, you can adjust the ```bootdelay``` and/or ```bootmenu_delay``` in U-Boot.

Sandisk's UltraFit range (model number SDCZ430) works nicely as a 'boot' drive in the internal USB connector for OpenWrt, uVirt and other lightweight distributions.

### SATA/RAID controllers
* InnoDisk EMPS-3401-C1 mPCIe to 4xSATA3.0 (Marvell chipset)
* InnoDisk EGPS-3401-C1 M.2 B/M to 4xSATA3.0 (Marvell chipset)
* IOCrest IO-M2F585-5I M.2 B/M (80mm) to 5xSATA3 (JMicron chipset)

## WiFi
Most ath9k and ath10k cards should work - see the [ath10k wiki](https://wireless.wiki.kernel.org/en/users/drivers/ath10k) for a list of hardware.

For WiFi6 / 802.11ax, the MediaTek 7915/6 family is the best option at this time.

* Compex WLE200N2 802.11n (ath9k)
* Compex WLE600VX 802.11ac 2x2 (ath10k)
* Compex WLE1216V5-20 802.11ac Wave 2 4x4 (ath10k)
* jjPlus JWX6058 802.11ac 2x2 + BT (ath10k) - must be in the mPCIe slot nearest the M.2/M for BT to work
* jjPlus JWX6052 802.11ac 3x3 (ath10k)
* AsiaRF AW7915-NPD 802.11ax 2x2 Dual Band Dual Concurrent (mt76)
* AsiaRF AW7916-NPD 802.11ax 3x3 (2x2 on 2.4GHz) Dual Band Dual Concurrent WiFi 6e 

Note that Intel WiFi cards (AX200 etc.) do not support access point mode (except for some P2P/WiFi direct use cases).

### Experimental ath11k cards

!!! Note

    Looking for a WiFi6/E solution? Use the MediaTek ones listed above.
    There are still some issues with the ath11k cards to solve, especially
    when used outside a Qualcomm/IPQ host system.

We have had some success getting these newer cards working. These cards are not fully supported by mainline
kernels or OpenWrt yet, so you will need to be able to compile your own OpenWrt and kernels to use them.

* Compex WLT639 (QCA6391) (requires a MiniPCIe or M.2 to Key E adaptor)
* Compex WLE3000H2 (QCN9074)
* Compex WLE3000H5 (QCN9074)
* Qualcomm PN02.1 reference card (requires "external" 5V power feed, e.g from a USB port)

## LTE and 5G
All M.2 modem cards should work - beware that the dual SIM feature is designed to work with cards that have
two sets of SIM pins - there is no SIM switcher on the Ten64 board. See the [LTE](/lte/) page for more details.

**PSA: DO NOT BUY A MODEM CARD THAT WAS FROM A LAPTOP UNLESS REALLY YOU KNOW WHAT YOU ARE DOING**

Most laptop modem cards have been customized by their respective OEMs to add a
proprietary unlock handshake. This is because the radio certification has been
done on the basis of the entire device (e.g laptop) rather than just the modem card,
hence it is in the regulatory interest of the manufacturer to ensure users
can't make 'unauthorized' changes that will void the device certification.

(See [wireless assembly guide](/applications/wireless-guide/) for more information).

Laptop OEM cards often have other quirks like operating 'flashless' (requiring firmware to
be uploaded to the card). Some of these quirks are known by the community and
can be worked around, but we recommend you avoid buying these cards completely.

### LTE

* Sierra EM7430/EM7455 Cat6
* Quectel EM12 Cat12

### 5G
5G cards in the 30x52mm form factor are supported, so far these have been tested (or reported by users) to work:

* SIMCOM SIM8200EA
* Fibocom FM150[^2]
* Quectel RM500Q-GL
* Quectel RM502Q-AE (note: this card only supports one SIM interface)
* Thales (Cinteron/Gemalto) MV31-W

    Note: Only the USB version (L30960-N6910) has been tested at this time.

* Quectel RM520N

Not recommended at this time:

* Sierra EM919x and EM7690: These require a signal from the host (VBUS_SENSE) to activate USB mode.

    On Ten64 Rev C this card will come up in PCIe mode (requires MHI drivers). 
    Future Ten64 revisions will have a header to activate USB mode when required.

* The Telit FN980m also works, but does not mount mechanically because it is slightly shorter and thicker than the others.

    Future Ten64 revisions will have a mounting hole for this card. 

## SFP and SFP+ transceivers

### 10 Gigabit
Practically any 10G SFP (excluding 10GBASE-LRM, see below) should work in the Ten64.

* Passive SFP+ "DAC" cables (generally, all should work)
* Ubiquiti Networks "UFiber" 10G MultiMode (UF-MM-10G)
* Finisar FTLX8571D3BCL 10GBASE-SR (Multimode)
* FiberStore/fs.com 10GBASE-SR (Multimode)
* FiberStore/fs.com SFP-10GLR-31 10GBASE-LR (Single mode)
*	FiberStore/fs.com 10G SFP+ Active Optical [Direct-attach] cable
* Mikrotik S+RJ10 10GBASE-T SFP+

    Due to the thermal characteristics of these modules (they get VERY hot), it is not recommended to have two of them
    installed at the same time, one or both of the modules may fail to link with the host due to the effect
    on the signal.

    A workaround for this is to use an SFP extender cable, such as [CVT-S/S-Cage](https://www.sfpcables.com/sfp-to-sfp-cage-with-3m-flat-cable-in-nylon-jacket-20cm-and-55cm-length-3256) to locate one or both
    10GBASE-T modules away from each other.

* 10GTek / SFPcables.com ASF-10G-T80 - 10GBase-T ONLY

    This 10GBase-T SFP runs much cooler than the Mikrotik, but the rate-adaptation method
    it's Broadcom chipset uses is not compatible with the Ten64, so you can only use
    it for 10GBase-T.

10GBASE-LRM (Long Range [Legacy] Multimode) transceivers are not supported - these require additional signal conditioning capabilities than the TI retimer can provide.

### 1 Gigabit

**Attention:** To use 1G SFP's you will need to need to flash a different BL2 file depending on desired configuration.
See [Using 1G SFP's](/network/sfp/#using-1g-sfps) for more information.

* 1000BASE-SX (850nm over MMF):
    * Ubiquiti Networks "UFiber" 1G MultiMode (UF-MM-1G)
* 1000BASE-T
    * Ubiquiti Networks UF-RJ45-1G
    * FiberStore/fs.com SFP-GB-GE-T
       * Remark: The 'SGMII' model is recommended as this allows link up at lower speeds (e.g 100Mbps)
    * 10GTek ASF-GE2-T
* VDSL 17a
    * Proscend 180-T
* GPON
    * Nokia (Alcatel Lucent) G-010S-A 3FE46541AA (OEM Source Photonics)
    * FS GPON-ONU-34-20BI (OEM Source Photonics)
    * Please note: 2.5G mode is not supported (as of 2022-06) on any existing Ten64 board due to retimer hardware limitations.
    * To access the management interface on these modules (192.168.0.10) without an active fiber WAN, you will need
to ignore the LOS (loss of) alarm. See [this patch](https://gitlab.com/traversetech/muvirt-lede/-/blob/muvirt_base_2102_2021_09_20/target/linux/arm64/patches-5.10/721-net-sfp-ignore-los-alcatel-gpon.patch).

NOT Compatible: Mikrotik S-85DLC05D due to invalid EEPROM contents - it fails
various checks in the Linux phylink/SFP attach code.


## Mini-ITX cases and power supplies
As noted in [power connector](/hardware/power-connector/), some older ATX power supplies will not work with no load on the 3.3V and 5V rails.

Cases and power supplies we know to work:

* In-Win IW-MS04-1 Mini-Server/NAS chassis 
* In-Win BQ656S (marketed as "Chopin Black") chassis with In-Win 150W OEM PSU
* In-Win B1
    * Tip: The SATA data cables for the 2.5" drive bays inside this case are quite short and will not reach a SATA controller installed in any of the card slots - you will want an extender cable, such as [StarTech SATAEXT30CM](https://www.startech.com/en-us/cables/sataext30cm).
* SeaSonic SS-250SU Flex ATX PSU
* Delta Electronics DPS150 and DPS250 Flex ATX PSU

Not compatible:

* U-NAS NSC200/201/400 have a mechanical interference issue between the mounting mechanism and the power connector on RevB boards - we are investigating changes in the layout for RevC
* Foxconn FSP150-50GLT power supply (powers down on 3.3V/5V no load)

[^1]: The LS1088 only has four lanes of PCIe - which is split x2 to M.2/M, x1 to PCIe Switch (then miniPCIe) and x1 to M.2/B, hence giving a full x4 connection isn't possible in our configuration.
[^2]: Very early samples of the Fibocom card required an external mode switch signal to select between PCIe and USB mode. This has now been resolved in recent versions - if you do not have PCIe drivers you will need to use an AT command to select USB mode.