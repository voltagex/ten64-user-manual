# Fan Control

The Ten64 has a Microchip [EMC2301](https://www.microchip.com/wwwproducts/en/EMC2301)
fan controller which supports fan control over I2C. When a 4-pin fan is installed,
you can control the fan speed with PWM.

Ten64 does not support speed control for 3-pin fans.

Ten64 desktop appliances have a [Noctua NF-A4x20 PWM](https://noctua.at/en/nf-a4x20-pwm)
installed which has a nominal speed of 5000 RPM.

## Fan control in U-Boot
Note: The fan control driver and command has not been upstreamed into U-Boot
yet. These commands may change into the future.

As the EMC2301 does not have a 'hardware' reset, U-Boot will reset the fan
speed to full (target=20000RPM) each boot.

* List fan speed

        => fan list
        Fan controller: emc2301@2f
         Number of fans: 1
         Fan 0 : 5442 RPM [target 20000]

* Enable PWM control for fan
    
    This must be done per-fan, otherwise fan speed commands from U-Boot will not take
    effect

        => fan enable_pwm emc2301@2f 0 1

* Set fan speed

        => fan set_speed emc2301@2f 0 3000
        (wait a few seconds to take effect)
        => fan list
        Fan controller: emc2301@2f
         Number of fans: 1
         Fan 0 : 3312 RPM [target 20000]

## Fan control in Linux
Fan control in Linux requires the (currently) out of tree [emc2301](https://gitlab.com/traversetech/ls1088firmware/traverse-sensors/-/tree/master/emc2301) driver.

This driver is present in our OpenWrt, μVirt and Debian kernel packages.

The device tree in the Ten64 firmware sets the emc2301 as a cooling device for the LS1088, and will step the fan between 3500-5500 RPM.
A method to override these settings from U-Boot variables (without having to change the device tree) will be provided in a future
firmware release.

The settings will appear in dmesg when the `emc2301` module is loaded:
```
emc2301 0-002f: EMC2301 detected
emc2301 0-002f: Have 1 fans configured in DT
emc2301 0-002f: Fan 0 Cooling step is 312 RPM, minimum 3000, max 5500 RPM
emc2301 0-002f: registering a cooling device
```
The emc2301 parameters can be controlled from sysfs (see [sensors](/hardware/sensors/) for a more general overview).

As hwmon devices are numbered in the order they are enumerated in, the numbering is not constant between distributions and kernels.
One way to identify the hwmon name is to lookup via the I2C platform device:

    $ ls /sys/devices/platform/soc/2000000.i2c/i2c-0/0-002f/hwmon/
    hwmon3

* Read fan speed

        $ cat /sys/class/hwmon/hwmon3/fan1_input
        4878

* Set fan target speed

    Note that the thermal subsystem will override the set fan speed if the CPU temperate increases

        $ echo 4000 > /sys/class/hwmon/hwmon3/fan1_target
        # Wait a few seconds for the fan speed to change
        $ cat /sys/class/hwmon/hwmon3/fan1_input
        4024