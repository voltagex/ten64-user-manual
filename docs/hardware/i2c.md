---
layout: page
title: I2C devices
permalink: /hardware/i2c/
---

There are three I2C busses on the Ten64 board:

- Primary/I2C1: Most sensors are on this bus
- SPD/RTC/I2C3: DDR SPD and Real time clock (on a separate bus to facilitate EFI RTC implementation in the future)

    <small>On RevA this was allocated exclusively to the TPM (Trusted Platform Module), on RevB the TPM moved to I2C1.</small>

- Peripheral/I2C4: For SFP+ EEPROM access

Both I2C1 and I2C4 are on the [GPIO header](/hardware/control-header/).

## List of I2C1 devices
| I2C Address | Part           | Description                                     |
|-------------|----------------|-------------------------------------------------|
| 0x00        | (Not a device) | SMBus broadcast address                         |
| 0x11        | PAC1934 U41    | Add in card current + 5V voltage                |
| 0x18        | EMC1704 U19    | CPU Temperature diode + Board temperature diode |
| 0x1A        | PAC1934 U20    | CPU I/O rails: DDR VDD, Vpp, Vtt, 1.8V/OVDD     |
| 0x27        | DS10DF410 U33  | Retimer for 10 Gigabit/SFP+                     |
| 0x29        | AT97SC3205T U23| Trusted Platform module                         |
| 0x2D        | USB5744 U42    | USB Hub (not applicable to RevD/OEM boards)     |
| 0x2F        | EMC2301 U59    | Fan control (PWM) and tachometer                |
| 0x4C        | EMC1813 U27    | Temperature monitor for QSGMII PHYs             |
| 0x61        | MP8869S        | CPU Core (1.0V) regulator                       |
| 0x63        | MP8869S        | 3.3V Regulator                                  |
| 0x65        | MP8869S        | 5.0V Regulator                                  |
| 0x76        | TCA9539 U39    | GPIO Expander for SFP, LTE control etc.         |
| 0x7E        | LPC804 U14     | Board [microcontroller](/hardware/microcontroller) |
