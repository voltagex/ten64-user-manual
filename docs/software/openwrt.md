---
layout: page
title: OpenWrt on Ten64
permalink: /software/openwrt/
---

There are two OpenWrt targets you can run on a Ten64:

* armsr, supporting disk devices (+NAND boot in the Traverse tree)
    * [OpenWrt Firmware Selector link](https://firmware-selector.openwrt.org/?version=SNAPSHOT&target=armsr%2Farmv8&id=generic)
* layerscape, supporting OpenWrt boot from the onboard NAND flash
    * [OpenWrt Firmware Selector link](https://firmware-selector.openwrt.org/?version=SNAPSHOT&target=layerscape%2Farmv8_64b&id=ten64-mtd)

As of September 2023, there are a few features missing from the upstream OpenWrt.org images:

* DPAA2 Crypto acceleration
* SFP status screen in LuCI
* Fan controller and sensor controller drivers (see note at bottom of this page on how to install them)

Links to OpenWrt images built by Traverse can be found at [archive.traverse.com.au](https://archive.traverse.com.au).

Our OpenWrt repository can be found at [gitlab.com](https://gitlab.com/traversetech/ls1088firmware/openwrt). Binaries can be downloaded
from our [archive server](https://archive.traverse.com.au/pub/traverse/ls1088firmware/openwrt/). 

For more information on the upstreaming effort, see the forum thread ["OpenWrt upstreaming update"](https://forum.traverse.com.au/t/openwrt-upstreaming-update/651).

### armsr (Arm SystemReady) target

The `armsr` target supports booting from disk devices (such as NVMe SSDs, SD cards, USB drives)
with a universal image that will work on standards compatible Arm hardware
(anything capable of booting EFI).

The Traverse development branch is based on `armsr`. To avoid unncecessary duplication
of resources, the Traverse tree also provides NAND support for `armsr`. This NAND
support is compatible with the Layerscape target in OpenWrt.org builds.

### Layerscape target
The `layerscape` target in OpenWrt was originally provided to support NXP development
boards with 'full system images'. Support for building NAND images for Ten64 was added
in OpenWrt 23.05.

## Installing OpenWrt to NAND

Our build process will generate binaries for both variants listed above.

To install OpenWrt into a NAND partition from the [recovery](/software/recovery/) system:

1. Check your MTD layout

Your U-Boot and Recovery need to implement the [dual-ubi partition scheme](/hardware/flash/), which is the default starting September 2020.

In U-Boot

In recovery:
    cat /proc/mtd

    dev:    size   erasesize  name
    mtd0: 00100000 00001000 "bl2"
    ....
    mtd9: 06c00000 00020000 "ubia"
    mtd10: 06c00000 00020000 "ubib"

2. Download the ubifs binary

The binary for NAND has the format ```openwrt-layerscape-armv8_64b-traverse_ls1088-```__```mtd-ubifs-nand.ubi```__

3. Write the binary to the ubifs partition

```
root@recovery000afa242400:/tmp# ubiformat /dev/mtd9 -f openwrt-layerscape-armv8_64b-traverse_ls1088-mtd-ubifs-nand.ubi
```

```
ubiformat: mtd9 (nand), size 113246208 bytes (108.0 MiB), 864 eraseblocks of 131072 bytes (128.0 KiB), min. I/O size 2048 bytes
libscan: scanning eraseblock 863 -- 100 % complete
ubiformat: 863 eraseblocks have valid erase counter, mean value is 6
ubiformat: 1 bad eraseblocks found, numbers: 819
ubiformat: flashing eraseblock 247 -- 100 % complete
ubiformat: formatting eraseblock 863 -- 100 % complete
```


## Compiling and installing sensor driver package for OpenWrt.org builds

You will need to download the SDK package (for example, `openwrt-sdk-23.05.0-rc4-armsr-armv8_gcc-12.3.0_musl.Linux-x86_64.tar.xz`).

It is *highly recommended* to download the SDK at the exact same time as you
download the image, otherwise the target kernel versions may not match,
especially for snapshot builds.

Then, you will need to add the Ten64 OpenWrt feed to the SDK and install
the sensor driver package into it.

```
cd openwrt-sdk-armsr-armv8_gcc-12.3.0_musl.Linux-x86_64
# This improves fetch performance for users located outside Europe (optional)
grep base feeds.conf.default | sed 's/git.openwrt.org/github.com/g' > feeds.conf
echo "src-git ten64 https://gitlab.com/traversetech/ls1088firmware/ten64-openwrt-feed.git" >> feeds.conf
./scripts/feeds update
./scripts/feeds install kmod-hwmon-traverse-sensors

# Configure the SDK to build our package only
cat <<'EOF' >>.config
# CONFIG_ALL is not set
# CONFIG_ALL_KMODS is not set
# CONFIG_ALL_NONSHARED is not set
# CONFIG_SIGNED_PACKAGES is not set
CONFIG_PACKAGE_kmod-hwmon-core=y
CONFIG_PACKAGE_kmod-hwmon-traverse-sensors=y
make defconfig

# Do the compilation
make -j$(nproc)
```

The compiled ipk will be available in `bin/targets/armsr/armv8/packages/`
```
~/openwrt-sdk-armsr-armv8_gcc-12.3.0_musl.Linux-x86_64 $ find bin/targets/ -name kmod-hwmon-traverse-sensors*
bin/targets/armsr/armv8/packages/kmod-hwmon-traverse-sensors_6.1.50+2022-05-29-795e6d6d-3_aarch64_generic.ipk
```