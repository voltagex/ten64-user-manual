---
layout: page
title: Bare Metal Appliance Store
permalink: /software/appliancestore/
---

# Bare Metal Appliance Store

__Bare Metal Appliance Store__ is a unique feature in the [recovery](/software/recovery/) firmware that simplifies the process of downloading and installing an operating system image.

Advantages:

* Download the latest available image for a distribution directly from the internet.
* Avoids having to use image writer software from another machine and having to use SD cards/USB flash etc.
* Self-hosting, no need for an external PXE server
    * Good for remote sites where the Ten64 is the sole internet connection (e.g using a cellular modem installed inside the Ten64)
* Some possibilies to script and customize the process for your needs
* Ability to setup your own image registry

Disadvantages:

* No access to distribution installer settings (e.g partitioning, locales, disk encryption)
    * Installed environment is generally similar to cloud server images

The bare metal appliance store shares the same backend as the [μVirt appliance store](https://gitlab.com/traversetech/muvirt/-/wikis/ApplianceStoreQuickStart) - images listings are maintained in the same [arm image registry](https://gitlab.com/traversetech/arm-image-registry). 

Note that the default registry has the ability to mark images as compatible/incompatible with hardware so not all appliances wil be presented for install. (You can use ``appstore-list --show-all`` to show all appliances in the registry)

## Demo - from power on to Debian login prompt
<asciinema-player speed="3" start-at="4" src="/asciinema/appstore2.rec"></asciinema-player>
<script src="/asciinema/asciinema-player.js"></script>

## Usage
Once you have booted into the recovery firmware, make sure you have internet access (e.g use `set-wan`):

```
recovery# set-wan eth8
```

You can then use __appstore-list__ to download the listings (if not already downloaded) and list available images:

``recovery#`` __``appstore-list``__

```
Lists not downloaded, will attempt to download
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  9498  100  9498    0     0  12852      0 --:--:-- --:--:-- --:--:-- 12852
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
100  1581  100  1581    0     0   2139      0 --:--:-- --:--:-- --:--:--  2142
registry        |appliance id                    |description
----------------|--------------------------------|--------------------------------
traverse        |traverse-openwrt-arm64          |Traverse OpenWrt build for ARM64 (disk devices)
traverse        |muvirt                          |muvirt (disk install)
officialdistro  |ubuntu-bionic                  *|Ubuntu Server 18.04 LTS ("Bionic Beaver")
officialdistro  |ubuntu-focal                   *|Ubuntu Server 20.04 LTS ("Focal Fossa")
officialdistro  |ubuntu-hirsute                 *|Ubuntu Server 21.04 ("Hirsute Hippo")
officialdistro  |opensuse-tumbleweed-jeos        |OpenSUSE Tumbleweed JeOS
officialdistro  |opensuse-leap-15.3-jeos         |OpenSUSE Leap 15.3 JeOS
officialdistro  |debian-stable                  *|Debian stable (10)
officialdistro  |debian-testing                 *|Debian testing (11)
officialdistro  |debian-unstable                *|Debian unstable (sid)
officialdistro  |fedora-34                      *|Fedora 34 Server/Cloud
[4 entries hidden due to incompatibility with this platform]
* = uses fixup manipulator to work on this platform
P = provisioner manipulator available
```

Then using __baremetal-deploy__ you can write the image to a disk:


__``baremetal-deploy ubuntu-hirsute /dev/nvme0n1``__
```
Checking compatibility...
NOTE: A manipulator will be used to fix compatibility issues between this image and hardware platform
ID: ten64-apt-manipulator            by Traverse Technologies
URL: https://archive.traverse.com.au/pub/arm-image-registry/manipulators/apt/apt-manipulator.lua


Appliance to deploy: ubuntu-hirsute - Ubuntu Server 21.04 ("Hirsute Hippo")
Device to deploy to: /dev/nvme0n1 (119.24GiB)
Downloading image
Image URL: https://cloud-images.ubuntu.com/server/releases/hirsute/release-20210513/ubuntu-21.04-server-cloudimg-arm64.img
Size of the image download: 519.0625M


! ! ! ! ! ! ! ! ! !
About to write image to device /dev/nvme0n1 if you don't want this, Ctrl-C now!
! ! ! ! ! ! ! ! ! !
[  189.256871]  nbd0: p1 p15
[  208.370261] block nbd0: NBD_DISCONNECT
[  208.374117] block nbd0: Disconnected due to user request.
[  208.379524] block nbd0: shutting down sockets

```

When a "manipulator" script is available, it will create a cloud-config file with your
desired defaults and do any required fixes:

<pre>
<b>What should the user password be?</b> hunter2
<b>Force user to change password on first login (y/n)? [n]</b>
<b>Change system locale (e.g en_AU, de_DE)? [leave blank to keep default image locale]</b>
<b>What should the hostname be? [leave blank to use appliance default]</b>
<b>Set the default upstream interface?</b> eth6
<b>Use DHCP6 to get an IPv6 address? On a home network that uses router advertisments for IPv6, answer "n" (y/n) [n]</b>
</pre>

```
Setting default user password: hunter2
Hostname:
netintf: eth6
Invoking run-manipulate --userpw=hunter2 --netintf=eth6 /tmp/manipulator-script.lua /dev/nvme0n1


------------------------------------------------------
Using manipulator script: /tmp/manipulator-script.lua
Resizing rootfs and adding swap partition
Root partition type is ext4
Setting up target chroot
[  226.883748] EXT4-fs (nvme0n1p1): mounted filesystem with ordered data mode. Opts: (null)
Adding swap to fstab
Distribution is ubuntu
Disabling flash-kernel
Running apt-get update
Executing: chroot /mnt/deployroot/ /bin/sh -l -c 'apt-get update'
Installing extra module package (DPAA2 drivers)
Executing: chroot /mnt/deployroot/ /bin/sh -l -c 'FK_MACHINE=none apt-get -y install linux-image-extra-virtual'
Setting default kernel cmdline
Running update-grub
Executing: chroot /mnt/deployroot/ /bin/sh -l -c update-grub
Have network interface: eth6
Making plain GRUB the default EFI application (disable SHIM)
#cloud-config
password: hunter2

Done!
------------------------------------------------------
Manipulator finished
Ensuring managed mode is set for Gigabit Ethernet
Setting legacy management mode for SFP+
```

You can then reboot and enjoy your new operating system.
The default username is usually the distribution name (`debian`,`fedora`,`ubuntu` etc)

Note that cloud-init processing on first boot can take time on some distributions.
If your first login attempt fails, wait until you see this line in the console:

<pre>
cloud-init[1832]: <b>Cloud-init v. 21.1-19-gbad84ad4-0ubuntu3 finished</b> at Tue, 08 Jun 2021 05:13:37 +0000. Datasource <b>DataSourceNoCloud</b> [seed=/dev/nvme0n1p15][dsmode=ne
</pre>