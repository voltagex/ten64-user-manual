# The Boot Process

The boot process from power on to reaching the U-Boot prompt can be summarized as:
1. After power is applied, the board microcontroller waits for the power regulators
to signal 'Power Good'

2. The microcontroller will then assert the power on reset line to the LS1088 (PORESET)

3. The LS1088 will read the selected RCW (Reset Configuration Word) source - QSPI or SD
and use this to configure the internal clocks/PLL. If the setup succeeds then it will load the
first stage bootloader (BL1) from the same source.

4. BL1 (typically ARM Trusted Firmware) will configure the DDR and then hand off to BL2
which loads and verifies the boot loader (BL33, e.g U-Boot).
This includes a change of privilege levels - Trusted Firmware executes (and remains resident) in EL3, while
everything from now on runs in EL2.

5. The boot loader loads and runs the necessary steps to load the operating system. This
might include setting up the Ethernet, PCIe, USB and other controllers.

It is also possible to boot directly in to U-Boot or another bootloader instead of using Trusted Firmware - for most
purposes this is now considered a legacy setup and no effort has been made to implement this.

See the [Trusted Firmware documentation](https://trustedfirmware-a.readthedocs.io/en/latest/getting_started/image-terminology.html)
for more information on BL1,2 and 3x images.
