---
layout: page
title: Board Support Package
permalink: /distributions/board-support-package/
---

Some functions on the Ten64 board require extra drivers or userspace workarounds to implement functionality
not implemented in shipping distribution kernels.

These include:

* Control of SFP+ pins (TXDISABLE)
* Implementing netdev LED triggers for SFP+ MACs/DPNIs
* Implementing sensor drivers not available in the Linux kernel
* Backporting drivers that have LS1088-relevant updates to earlier kernels.

The main board support package is the [Traverse Board Support](https://gitlab.com/traversetech/ls1088firmware/traverse-board-support) package
which is available for Debian (+derivates) and openSuSE.

Additional packages include:

* [Traverse Sensors DKMS/KMP](https://gitlab.com/traversetech/ls1088firmware/traverse-sensors)
* [gpio-mpc8xxx backport for openSuSE Leap 15.1 and 15.2](https://build.opensuse.org/package/show/home:mcbridematt/gpio-mpc8xxx-test) (needed for kernel <5.4>)

These functions are integrated into our [OpenWrt](/software/openwrt/) tree for distributions based on it.

## Repositories
### DEB/APT
This repository is currently experimental and unsigned.
Add to /etc/apt/sources.list:

```
deb [trusted=yes] https://archive.traverse.com.au/pub/traverse/debian-experimental/ buster-support main
apt-get update
apt-get install traverse-board-support
```

### openSuSE

Leap 15.1:
```
zypper addrepo https://download.opensuse.org/repositories/home:mcbridematt/openSUSE_Leap_15.1/home:mcbridematt.repo
zypper refresh
zypper install traverse-board-support gpio-mpc8xxx traverse-sensors
```

Leap 15.2:
```
zypper addrepo https://download.opensuse.org/repositories/home:mcbridematt/openSUSE_Leap_15.2/home:mcbridematt.repo
zypper refresh
zypper install traverse-board-support gpio-mpc8xxx traverse-sensors
```