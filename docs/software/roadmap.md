---
layout: page
title: Firmware and Software roadmap
permalink: /software/roadmap/
---

Given the newness of the Ten64 hardware, there are a couple of software developments that are
of interest.

## Firmware
### Persistent (non-volatile) EFI Variable support in U-Boot via (secure-world?) application
* When: Late 2020/Early 2021

One major shortfall of the EFI implementation in U-Boot is lack of persistent variable storage at runtime (after the OS has booted).
This requires a runtime service that can interface with the variable storage medium (e.g QSPI flash).

The main proposed method for inclusion in U-Boot runs EDK2's variable as an OP-TEE
secure world application. The current implementation uses the RPMB (replay protected memory block)
of an eMMC flash (and requires a "normal-world" supplicant) whereas we expect to implement storage with the QSPI flash.

[Previous attempts](https://patchwork.ozlabs.org/project/uboot/cover/20190905082133.18996-1-takahiro.akashi@linaro.org/) have been made to
implement persistent variables in U-Boot proper but the surface impact on U-Boot is too large to upstream.

Unlike the eMMC/RPMB approach, which requires a supplicant in the "normal" world (in U-Boot and Linux), the
EFI variable handler will take over ownership of the QSPI controller which might require some changes
to how QSPI is handled in U-Boot.

Another proposed approach (that could be implemented in the immediate future) is to only have a read-only variable service and allow updates by EFI capsules that are processed by U-Boot at
startup.

More information:

* U-Boot patch series [EFI variable support via OP-TEE](https://patchwork.ozlabs.org/project/uboot/cover/20200515195306.522742-1-ilias.apalodimas@linaro.org/)

* Slide 14 of [UEFI Secure Boot on U-Boot](https://www.elinux.org/images/d/db/2019.08.23_UEFI_Secure_Boot_on_U-Boot.pdf)

* [Patch series for capsule-based variable updates](https://patchwork.ozlabs.org/project/uboot/cover/20200327052800.11022-1-xypron.glpk@gmx.de/)
### EFI RTC (Real time clock)
* When: Near future

Many Linux distributions only build a limited number of I2C RTC drivers by default which
can cause the RTC on the board not to be used - by implementing an EFI RTC service these issues
will be alleviated.

### EFI Secure Boot
* When: Near future (depends on acceptance of U-Boot secure boot patchset)

EFI secure boot is required for some distribution bootloaders (such as `shim`) to work - even if there is
no secure boot enforcement.

Until EFI variable support is implemented the secure boot keys will need to be set by the user
manually.

* Presentation [UEFI Secure Boot on U-Boot](https://www.elinux.org/images/d/db/2019.08.23_UEFI_Secure_Boot_on_U-Boot.pdf)

### Firmware and configuration updates via EFI capsules
* When: pending further investigation

Updating DPAA2 management complex firmware along with DPC and DPL configurations via EFI capsules
would be a useful feature. Similarly for U-Boot, ARM Trusted Firmware and auxiliary images such as
the recovery image.

As MC firmware versions and DPC/DPL are customer specific (customers may only validate against certain DPDK and MC firmware versions)
we do not expect to participate in any centralized services such as [LVFS](https://fwupd.org/) - but there could be a facility
in the [recovery image](/software/recovery/) to download latest versions.

### ACPI support
* When: TBA

The ARM server boot standard calls for ACPI as the method for passing hardware information
to the operating system. At the moment Ten64 uses device tree and we expect to remain this way
for the foreseeable future (given our target as embedded/appliance rather than client or datacenter) - if you have a need to run an ACPI-only operating system - please contact us.

There is some work in U-Boot at the moment to allow generation of ACPI tables from device tree models
(which are used by U-Boot for it's device model) - which may speed this up.

### EDK2/TianoCore support
* When: TBA

EDK2/TianoCore implementations do exist for other Layerscape SoC's (LS1043,LS1046, LS2088 and LX2160).
Most of these use DT while the LX2160 also has an ACPI implementation.

We expect any EDK2 implementation we commission would use ACPI. EDK2 will share Trusted Firmware and
variable storage (via OP-TEE) with U-Boot which should reduce the implementation size vs earlier implementations.

## Linux kernel
### Phylink management for DPAA2 DPNI's/DPMACs and SFP's
* When: End 2020

Until recently, Linux had no standard method of managing PHYs connected to an Ethernet adaptor - on other
platforms these tend to be abstracted away by firmware (e.g ACPI), by intermediate PHY devices or managed entirely
by userspace.

[Phylink](https://www.kernel.org/doc/html/latest/networking/sfp-phylink.html) is the new PHY management subsystem
for Linux which also provides management of SFP transceivers.

At the time of writing, a Phylink implementation exists for other DPAA2 interface types such as RGMII and 10G on the LX2160, implementation
of QSGMII and XFI interfaces is required on the LS1088.

Similarly, LEDs can't currently be linked to DPAA2 network devices in the device tree as the phy handles aren't "real".

Further information:

* [ELC 2018 presentation: From Ethernet MAC to the Link partners](https://events19.linuxfoundation.org/wp-content/uploads/2017/12/chevallier-tenart-from-the-ethernet-mac-to-the-link-partner.pdf)

* [Phylink and SFP: Going beyond 1G copper](http://vger.kernel.org/lpc_net2018_talks/phylink-and-sfp-slides.pdf)

## Integrated hardware flow offload with AIOP
* When: Wishlist item/To be scoped

The AIOP engine inside the LS1088 provides an exciting opportunity to offload network flows from the SoC, together with a generic flow offload framework
in Linux many issues present in vendor-specific offload implementations (often seen in residential gateway SoCs) can be avoided.

This would provide an acceleration path for many router distributions that work around iptables/nftables/etc. without the pain of moving to solutions such
as DPDK.

Further information:

* [OpenWrt Summit 2018 presentation on flow offloading (Hauke Mehrtens)](https://openwrtsummit.files.wordpress.com/2018/11/hauke-mehrtens.pdf)
* [DPAA2 Overview](/network/dpaa2overview/)
