# μVirt

μVirt (US-ASCII: muvirt) is a OpenWrt based distribution that acts a
virtual machine host, targetted at Virtual Network Function (VNF) use cases,
such as multi-tenant routers and deploying value add services such as local IoT gateways.

It is designed to use a little host hardware resources as possible, reducing
hardware requirements and allowing VMs to use the host hardware to the full extent possible.

A key goal is to provide an environment to test acceleration features such as DPAA2, DPDK-OvS without
the constraints of an existing stack.

muvirt source can be obtained at [Gitlab](https://gitlab.com/traversetech/muvirt).
