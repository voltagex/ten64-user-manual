# XDP on Ten64 / NXP DPAA2

NXP DPAA2 was one of the first ARM networking platforms to support XDP, and in general you can develop
and deploy XDP programs in the same way as you can on x86.

If you are looking for a tutorial on XDP, [xdp-project/xdp-tutorial](https://github.com/xdp-project/xdp-tutorial)
is a good place to start.

However, you may need to employ workarounds on certain distributions:

## General requirements

* Ensure `CONFIG_BTF` is enabled in your kernel configuration.

## XDP on Debian ARM64

The `gcc-multilib` package is not available on arm64. This provides `/usr/include/asm` which
is used in the compilation of eBPF/XDP programs.

To work around this, do:

    sudo ln -s /usr/include/aarch64-linux-gnu/asm asm


